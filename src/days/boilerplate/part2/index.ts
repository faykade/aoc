import { getParsedFile } from '@/utils';
import * as path from 'path';

const getStructuredData = (parsedFile: string[]) => {
  return parsedFile;
};

export const runPart2 = () => {
  const parsedFile = getParsedFile(path.join(__dirname, 'input.txt'));

  const structuredData = getStructuredData(parsedFile);
  console.log('structuredData: ', structuredData);
};
