import { Day } from '@/types';
import { runPart1 } from './part1';
import { runPart2 } from './part2';

const day: Day = {
  runPart1,
  runPart2,
};

export default day;
