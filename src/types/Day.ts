export interface Day {
  runPart1: () => void;
  runPart2: () => void;
}
