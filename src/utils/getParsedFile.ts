import * as fs from 'fs';

export const getParsedFile = (filePath: string): string[] => {
  try {
    const fileContents = fs.readFileSync(filePath, 'utf8');
    console.log('fileContents: ', fileContents);
    return fileContents.split('\r\n');
  } catch (err) {
    console.error('Error reading input file: ', filePath);
    return [];
  }
};
