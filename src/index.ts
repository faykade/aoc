import { Day } from './types';
// MORE IMPORTS HERE

const days: Record<number, Day> = {
  // MORE DAYS HERE
};

async function runDay(dayId: number, part?: number) {
  const day = days[dayId];
  if ((part && part === 1) || !part) {
    console.log('=========================');
    console.log('Part 1');
    console.log('-------------------------');
    day.runPart1();
    console.log('=========================');
  }

  if ((part && part === 2) || !part) {
    console.log('=========================');
    console.log('Part 2');
    console.log('-------------------------');
    day.runPart2();
    console.log('=========================');
  }
}

const params = process.argv.splice(2);
if (params.length) {
  const day = parseInt(params[0]);
  const part = params.length > 1 ? parseInt(params[1]) : undefined;
  runDay(day, part);
} else {
  console.log(`Usage: npm run start [day] [problem]`);
  console.log(`Available days: [ ${Object.keys(days).join(', ')} ]`);
}
