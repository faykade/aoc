const { promises, cpSync } = require('fs');
const path = require('path');
const { exit } = require('process');

if (process.argv.length !== 3) {
  console.info('Usage: npm run setup {day}');
  exit(0);
}
const day = parseInt(process.argv[2]);

if (day < 1 || day > 25) {
  console.warn('Please enter a day between 1 and 25');
  exit(0);
}

const startPath = process.cwd();

(async function () {
  console.log(`Setting up day ${day}`);
  await createCodeFiles();
  await updateIndex();
  console.log('All done!');
})();

async function createCodeFiles() {
  const templateFolder = path.join(startPath, 'src', 'days', 'boilerplate');
  const codeFolder = path.join(startPath, 'src', 'days', day.toString());
  cpSync(templateFolder, codeFolder, { recursive: true });
}

async function updateIndex() {
  const indexPath = path.join(startPath, 'src', 'index.ts');
  const contents = (await promises.readFile(indexPath, { encoding: 'utf8' }))
    .replace(
      '// MORE IMPORTS HERE',
      `import { default as day${day} } from './days/${day}';
// MORE IMPORTS HERE`,
    )
    .replace(
      '// MORE DAYS HERE',
      `${day}: day${day},
  // MORE DAYS HERE`,
    );

  console.log('  Updating index');
  await promises.writeFile(indexPath, contents);
}
