## Usage

Use with advent of code. To create the boilerplate for a new day, use `npm run setup {dayNumber}` where `dayNumber` is anything between `1` and `25`.

To run a day, use `npm start {dayNumber} {problemNumber}` where `dayNumber` exists, and `problemNumber` is either `1` or `2`
